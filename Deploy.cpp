//stl
#include <iostream>
#include <vector>
#include <sstream>
#include <stack>
#include <string>
#include <algorithm>

//Proiect
#include "AiEngine.h"
#include "Deploy.h"
#include "SuperRegion.h"
#include "tools/StringManipulation.h"
#include "Fight.h"
#include "Map.h"

Deploy::Deploy(AiEngine* AI):
	p_ai(AI)
{}

Deploy::~Deploy()
{
}

void Deploy::setDisponibleArmies(const int& nbArmies)
{
	m_disponible_armies = nbArmies;
}

void Deploy::resetDeploy()
{
	enemies_defended = 0;
}

/**
 * Function used to rank my partial SuperRegions and to get the perfect target
 * to extend to ( we chose the fastest sr that can be conquered)
 */
double Deploy::getSuperRegionsToExtendToValue(const SuperRegion & sr)
{
	int underControlRegions = 0, armiesInSuperRegion = 0;
	for (size_t i = 0; i < sr.size(); ++i) 
	{
		if ((*Map::getInstance().getRegions())[sr.getRegion(i)].getOwner() == ME)
		{
			++underControlRegions;
		}
		armiesInSuperRegion += (*Map::getInstance().getRegions())[sr.getRegion(i)].getArmies();
	}

	if(underControlRegions / sr.size() == 1) {
		return -1;
	}
	SuperRegion newSr = sr;
	if(newSr.hasWasteland()) {
		return 10 + (sr.size() -underControlRegions);
	}
	if( newSr.getReward() == 0) {
		return 20;
	}

	return sr.size() - underControlRegions;
	
}

/**
 * This function uses the ranking system above and return a stack with
 * superRegions with the most usefull to extend in the top
 */
std::stack<int> Deploy::getSuperRegionsToExtendTo()
{

	std::vector<SuperRegion> superRegions = Map::getInstance().getMyPartialSuperRegions();
	std::sort(superRegions.begin(), superRegions.end(), 
		[&](const SuperRegion& lhs, const SuperRegion& rhs)
		{
			return getSuperRegionsToExtendToValue(rhs) < getSuperRegionsToExtendToValue(lhs);	
		});

	std::stack<int> targets;
	
	for (size_t i = 0; i < superRegions.size(); ++i){
		targets.push(superRegions[i].getID());
	}
	return targets;
}


/**
 * Function to expand in a superRegion in which I already have at least one Region
 * Used the stack of prioritized superRegions and then attack the regions of this 
 * from all the possible directions. Every attack decision is memorised and given
 * to the AiENgine
 */
std::vector<std::pair<int,int> > Deploy::deployArmiesForExtend()
{
	//std::cerr<<"I extend"<<std::endl;
	std::stack<int> targets = getSuperRegionsToExtendTo();
	std::vector<std::pair<int,int> > deploies;

	while( m_disponible_armies > 0 && !targets.empty() ) 
	{
		int super_region_to_conquer = targets.top();
		targets.pop();
		//Get my regions with direct acces to the target neutral teritories
		std::vector<Region> bases = Map::getInstance().getSuperRegNotMineNb(super_region_to_conquer);
		//Give me neutral regions from a SuperRegion
		std::vector<Region> neutrals_regions = Map::getInstance().getEmptyFromSuperReg(super_region_to_conquer);
		for( size_t i = 0; i < neutrals_regions.size(); ++ i) 
		{
			for( size_t j = 0; j < bases.size(); j++) {
				if(bases[j].isNeighbor(neutrals_regions[i].getID())) {
					int necessary_armies = getNecessaryArmiesToConquerRegion(neutrals_regions[i]);
					int resourcesLeft = bases[j].getArmies() - 1 - p_ai->getInitialSuplyUsed(bases[j].getID());
					int useResource = std::min(resourcesLeft, necessary_armies);
					if( useResource != 0 ) {
						p_ai->takeInitialSuply(bases[j].getID(),useResource);
					}
					necessary_armies -= useResource;

					int armies = std::min(necessary_armies,m_disponible_armies);
					//Make deploy for conquer that neutral region
					if(armies <= m_disponible_armies) {
						p_ai->useToExtend(bases[j].getID(), neutrals_regions[i].getID());
					}
					m_disponible_armies -= armies;
					if(armies > 0)
					{
						std::pair<int, int> solution(bases[j].getID(), armies);
						deploies.push_back(solution);
					}
					break;
				}
			}
			if(m_disponible_armies == 0) {
				break;
			}
		}
	}
	p_ai->setNbArmies(m_disponible_armies);
	return deploies;
}

/**
 * This function check if the Region to be conquer is neutral or owned by
 * an enemy and expect a possible deploy
 */
int Deploy::getNecessaryArmiesToConquerRegion(Region region) 
{
	Fight f;
	if( region.getOwner() == NEUTRAL)
		return f.getAttackersNeededToCapture(region.getArmies());
	return f.getAttackersNeededToCapture(region.getArmies() + p_ai->enemy_expected_deploy);
}
/**
 * Get the best zone to defend an enemy
 * The function defends only against one enemy. All the armies used to defend
 * are marked as used. Also armies used to suply. If the defend has succed the
 * region is marked as safe
 */
std::vector<std::pair<int,int> > Deploy::deployArmiesForDefend()
{
	Fight f;
	//std::cerr<<"I defend\n";
	std::vector<std::pair<int,int> > sol;
	std::set<int> enemies = Map::getInstance().getEnemyNeighbors();
	if(enemies.size() == 0) {
		return sol;
	}
	int enemyToDefend = getEnemyToDefendAt(enemies, enemies_defended);
	enemies_defended++;
	int armies_comming = (*Map::getInstance().getRegions())[enemyToDefend].getArmies() + p_ai->enemy_expected_deploy - 1;
	std::vector<int> myZones = enemyDangerZones(enemyToDefend);
	int necessary_armies = f.getDefendersNeededToKeep(armies_comming);
	for(size_t i = 0; i < myZones.size(); ++i)
	{
		Region myRegion = (*Map::getInstance().getRegions())[myZones[i]];
		if( myRegion.getArmies() < necessary_armies ) {
			int neededSuply = necessary_armies - myRegion.getArmies();
			int possibleSuply = checkIfReinforcementCouldSuply(myZones[i]);
			neededSuply -= possibleSuply;
			if(neededSuply > m_disponible_armies) {
				continue;
			}
			p_ai->askForSuply(myZones[i]);
			if(neededSuply  <= 0)
				continue;
			m_disponible_armies -= neededSuply;
			std::pair<int,int> p(myZones[i],neededSuply);
			p_ai->takeInitialSuply(myZones[i],myRegion.getArmies()-1);
			sol.push_back(p);
			p_ai->markAsSafe(myZones[i]);
		}
	}
	p_ai->setNbArmies(m_disponible_armies);
	return sol;

}
/**
 * Return my regions that can be atacked by an enemy this turn
 */
//TO DO--> we can use enemy pattern to see the zone where he attack
std::vector<int> Deploy::enemyDangerZones(const int& enemy) {
	std::vector<int> myZones;
	Region opponent = (*Map::getInstance().getRegions())[enemy];
	for(int j = 0; j < opponent.getNbNeighbors(); ++j)
	{
		Region reg = (*Map::getInstance().getRegions())[opponent.getNeighbor(j)];
		if( reg.getOwner() == ME ) {
			myZones.push_back(reg.getID());
		}
	}
	return myZones;
}

/**
 * After a sorting get the most powerfull(dangerous) enemy to defend from his
 * possible attack
 */
int Deploy::getEnemyToDefendAt(std::set<int> set_enemies, int enemies_defended)
{
	std::vector<int>enemies;
	std::copy(set_enemies.begin(), set_enemies.end(), std::back_inserter(enemies));
	std::sort(enemies.begin(), enemies.end(), 
		[&](const int& lhs, const int& rhs)
		{
			return (*Map::getInstance().getRegions())[lhs].getArmies() > (*Map::getInstance().getRegions())[rhs].getArmies();	
		});
	return enemies[enemies_defended];
}

/**
 * This function check if a near zone could come and help a defending zone
 * If this is posible the transfer is marked and sendend to the AiEngine
 */
int Deploy::checkIfReinforcementCouldSuply(const int& my_region)
{
	int suply = 0;
	Region myRegion = (*Map::getInstance().getRegions())[my_region];
	for( int i = 0; i < myRegion.getNbNeighbors(); ++i)
	{
		Region my_neighbor = (*Map::getInstance().getRegions())[myRegion.getNeighbor(i)];
		if( my_neighbor.getOwner() != ME )
			continue;
		bool valid = true;
		for( int j = 0; j < my_neighbor.getNbNeighbors(); ++j)
		{
			if((*Map::getInstance().getRegions())[my_neighbor.getNeighbor(j)].getOwner() == ENEMY )
			{
				valid = false;
				break;
			}
		}
		if( valid )
		{
			suply += getSuplyLeft(my_neighbor);
			if(getSuplyLeft(my_neighbor) > 0) {
				std::vector<int> transfer;
				transfer.push_back(my_neighbor.getID());
				transfer.push_back(my_region);
				transfer.push_back(getSuplyLeft(my_neighbor));
				p_ai->addSuply(transfer);
				p_ai->takeInitialSuply(my_neighbor.getID(), getSuplyLeft(my_neighbor));
			}

		}

	}
	return suply;
}

/**
 * This function search a vulnerable enemy and attack him from a powerfull region
 * mine
 */
std::vector<std::pair<int,int> > Deploy::deployArmiesForAttackEnemies()
{
	Fight f;
	//std::cerr<<"Deploy for attack\n";
	std::vector<std::pair<int,int> > solution;
	std::vector<std::vector<Region> > my_out_regions = Map::getInstance().getMyExteriorRegions();
	//std::vector<Region> myRegions;
	std::sort(my_out_regions.begin(), my_out_regions.end(), 
		[&](const std::vector<Region>& lhs, const std::vector<Region>& rhs)
		{
			return getSuplyLeft(lhs[0]) > getSuplyLeft(rhs[0]);	
		});
	for(size_t i = 0; i < my_out_regions.size(); ++ i)
	{
		std::vector<Region> possibleDeploy = my_out_regions[i];
		Region myRegion = possibleDeploy[0];
		int armiesDisponible = getSuplyLeft(myRegion);
		for( size_t i = 1; i < possibleDeploy.size(); i++) {
			Region target = possibleDeploy[i];
			if(target.getOwner() == NEUTRAL)
			{
				continue;
			}
			int armiesNeeded = f.getAttackersNeededToCapture(target.getArmies() + p_ai->enemy_expected_deploy);
			if(armiesNeeded - armiesDisponible <= m_disponible_armies )
			{
				std::pair<int,int> m(myRegion.getID(),armiesNeeded - armiesDisponible);
				if(armiesNeeded < armiesDisponible)
				{
					p_ai->takeInitialSuply(myRegion.getID(),armiesNeeded);
					p_ai->useToAttackEnemy(myRegion.getID(),target.getID());
						continue;
					//MARK FOR ATTACK
				}
				p_ai->takeInitialSuply(myRegion.getID(),armiesDisponible);
				solution.push_back(m);
				m_disponible_armies -= (armiesNeeded - armiesDisponible);
				p_ai->useToAttackEnemy(myRegion.getID(),target.getID());
				
			}
		}
	}
	p_ai->setNbArmies(m_disponible_armies);
	return solution;

}

/**
 * Get all the armies that can be used from a region
 */
int Deploy::getSuplyLeft(Region region)
{
	int id = region.getID();
	return region.getArmies() - p_ai->getInitialSuplyUsed(id) -1;
}

/**
 * Deploy armies to defend at other zones threathen by enemy
 */
std::vector<std::pair<int, int> > Deploy::deployArmiesForBackup()
{
	std::vector<std::pair<int, int> > solution;
	std::set<int> enemies = Map::getInstance().getEnemyNeighbors();
	for( size_t i = 1; i < enemies.size(); ++ i)
	{
		std::vector<std::pair<int, int> > partial_solution = deployArmiesForDefend();
		solution.insert( solution.end(), partial_solution.begin(), partial_solution.end() );
	}
	return solution;
}
/**
 * Deploy all the armies left
 * in the moset vulnerable region
 */
std::vector<std::pair<int, int> > Deploy::deployLastArmies()
{
	//std::cerr<<"I deploy last\n";
	std::vector<std::pair<int,int> > solution;
	std::vector<std::vector<Region> > my_out_regions = Map::getInstance().getMyConflictZones();
	std::sort(my_out_regions.begin(), my_out_regions.end(), 
		[&](const std::vector<Region>& lhs, const std::vector<Region>& rhs)
		{
			return getSuplyLeft(lhs[0]) < getSuplyLeft(rhs[0]);	
		});
	std::vector<Region> possibleDeploy = my_out_regions[0];
	Region myRegion = possibleDeploy[0];
	std::pair<int, int> my_final_deply(myRegion.getID(), m_disponible_armies);
	p_ai->setNbArmies(0);
	solution.push_back(my_final_deply);
	return solution;

}