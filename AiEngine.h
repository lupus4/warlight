#ifndef AIENGINE_H
#define AIENGINE_H

//stl
#include <vector>
#include <unordered_map>

//project
#include "Map.h"
#include "Deploy.h"
#include "Battle.h"

/**
 * This is the brain of the bot. Recieve task from the bot and take calls based
 * on the result of deploy and battle engine
 */
class AiEngine
{
public:
	AiEngine();
	virtual ~AiEngine();
	//Mark that initial resources of a region were used for a task
	void takeInitialSuply(const int& myRegion, const int& nbArmies);
	//Check if a region has initial resources disponible
	int getInitialSuplyUsed(const int& myRegion);
	//Mark a region for attack neutral
	void useToExtend(const int& myRegion, const int& neutralRegion);
	int getNbArmiesLeft() { return m_nbArmies; }
	int enemy_expected_deploy = 5;
	void askForSuply(const int& myRegion);
	void setNbArmies(const int& armies) { m_nbArmies = armies;}
	std::vector<std::pair<int, int> > deployArmies();
	std::vector<std::vector<int> > attack();
	void useToAttackEnemy(int source, int destination);
	void markAsSafe(const int& region);
	bool isSafe(const int& region);
	void addSuply(std::vector<int> suply);



private:
	/** Vectors with the regions scheduled for a task */
	std::vector<std::vector<int> > usedToExtend;
	std::vector<std::vector<int> > usedToAttackEnemy;
	std::vector<std::vector<int> > transfer;
	std::unordered_map<int,int> takenInitialSuply;
	std::set<int> safeRegions;
	/** Engines used to take calls */
	Deploy m_deployEngine;
	Battle m_battleEngine;
	int m_nbArmies;
	/** The number of round */
	/** TO DO diferentiate EARLY_GAME MID_GAME LATE_GAME */
	int round = 1;
};

#endif