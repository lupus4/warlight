#ifndef FIGHT_H
#define FIGHT_H

/**
 * Class used for computing the restults of a fight.
 * It gives necessary information for defending a region
 * or conquer another.
 */
class Fight
{
public:
	int getDefendersDestroyed(const int& nbAttackers);
	int getAttackersDestroyed(const int& nbDefenders);
	int getAttackersNeededToCapture(const int& nbDefenders);
	int getDefendersNeededToKeep(const int& nbAttackers);

private:
	float getDefendersDestroyedByLuck(const int& nbAttackers);
	float getAttackersDestroyedByLuck(const int& nbDefenders);
	/** The factor of luck used in game */
	double luck = 0.16;

};


#endif