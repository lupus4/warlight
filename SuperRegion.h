#ifndef SUPERREGION_H_INCLUDED
#define SUPERREGION_H_INCLUDED

// stl
#include <vector>

// project
#include "main.h"
#include "Region.h"

/**
 * The class used for describing a SuperRegion. Added ID functionality
 * verification for wastelands
 */
class SuperRegion
{
public:
	SuperRegion();
	SuperRegion(const int& pReward);

	virtual ~SuperRegion();
	void addRegion(const int& region);
	size_t size() { return regions.size(); }
    int getRegion(int i) { return regions[i]; }
    int getID() { return m_id; }
	size_t size() const { return regions.size(); }
    int getRegion(int i) const { return regions[i]; }
    int getID() const { return m_id; }
    void setID(int id) { m_id = id; }
    int getReward() {return reward; }
    bool hasWasteland();

private:
	std::vector<int> regions;
	int reward;
    int m_id;
};

#endif // SUPERREGION_H_INCLUDED