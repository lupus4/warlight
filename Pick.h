#ifndef PICK_H
#define PICK_D

/**
 * The class used for picking a strating region by ranking the
 * possible picks. The strategy is to have many SuperRegion 
 * one near another.
 */
class Pick
{
public:
	int pickRegion(std::vector<int> startingRegionsReceived) ;
	
private:
	double getScore(const unsigned& noRegion);
	int rankingSearchForBestRegion(std::vector<int> startingRegionsReceived);
};

#endif