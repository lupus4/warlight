//stl
#include <iostream>
#include <cmath>
//proiect
#include "Fight.h"

/**
 * Get number of defending armies destroyed by luck
 */
float Fight::getDefendersDestroyedByLuck(const int& nbAttackers) 
{
	int defendersDesroyed = 0;
	for(int i = 1; i <= nbAttackers; i++)
	{
		int chance = std::rand() % 100;
		if(chance < 60)
		{
			defendersDesroyed++;
		}
	}
	return luck * defendersDesroyed;
}

/**
 * Get thenumbers of attackers destroyed by luck
 */
float Fight::getAttackersDestroyedByLuck(const int& nbDefenders)
{
	int attackersDestroyed = 0;
	for(int i = 1; i <= nbDefenders; i++)
	{
		int chance = std::rand() % 100;
		if(chance < 70)
		{
			attackersDestroyed++;
		}
	}
	return luck * attackersDestroyed;
}

int Fight::getDefendersDestroyed(const int& nbAttackers)
{
	if(nbAttackers == 1)
	{
		return 0;
	}
	float value1 = 0.6 * nbAttackers;
	float m =  value1 * (1 - luck) + getDefendersDestroyedByLuck(nbAttackers);
	int a = std::round(m);
	return a;
}


int Fight::getAttackersDestroyed(const int& nbDefenders)
{
	double value1 = 0.7 * nbDefenders;
	float m = value1 * (1 - luck) + getAttackersDestroyedByLuck(nbDefenders);
	int a = std::round(m);
	return a;
}

/**
 * Get the number of armies you need to conquer a regions with known 
 * number of armies
 */
int Fight::getAttackersNeededToCapture(const int& nbDefenders)
{
	/** Sometimes the game not give the same result so i add to be 
	 * certain :D
	 */
	for( int i = nbDefenders; i <= 2*nbDefenders; i++ )
	{
		if(getDefendersDestroyed(i) >= nbDefenders)
		{
			if(nbDefenders >= 4) i++;
			return i;
		}
	}
	return 2*nbDefenders + 1;
}

/**
 * Get the numbers of armies you need to defend from an attacker with
 * known number of armies
 */
int Fight::getDefendersNeededToKeep(const int& nbAttackers)
{
	for( int i = 1; i <= nbAttackers; i++ )
	{
		if(getDefendersDestroyed(nbAttackers) < i)
		{
			return i;
		}
	}
	return nbAttackers + 1;
}