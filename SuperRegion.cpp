//project
#include "SuperRegion.h"
#include "Map.h"

SuperRegion::SuperRegion()
	: reward(0)
{
}

SuperRegion::SuperRegion(const int& pReward)
	: reward(pReward)
{
}

SuperRegion::~SuperRegion()
{
}

void SuperRegion::addRegion(const int& region)
{
	regions.push_back(region);
}
bool SuperRegion::hasWasteland(){
	for(size_t i = 0; i < regions.size(); ++i) {
		if((*Map::getInstance().getRegions())[regions[i]].isWasteland()) {
			return true;
		}
	}
	return false;
}