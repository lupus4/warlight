//stl
#include <map>
#include <vector>

//Proiect
#include "Pick.h"
#include "Map.h"

/*Ranking function for picking a region considering the bonus-size raport,
  no of neighbors, possibility to be conquered fast*/
double Pick::getScore(const unsigned& noRegion)
{
    std::vector<Region>* regions = Map::getInstance().getRegions();
    std::vector<SuperRegion>* superRegions = Map::getInstance().getSuperRegions();
    std::map<int,int> supers;
    Region region = regions->at(noRegion);
    SuperRegion super_region = superRegions->at(region.getSuperRegion());
    int bonus = super_region.getReward();
    int super_region_size = super_region.size();
    int neighbors_same_region = 0;
    int super_neighbors = 0;
    int wastes = 0;

    for (int i = 0; i < region.getNbNeighbors(); ++ i)
    {
        if (regions->at(region.getNeighbor(i)).getSuperRegion() == region.getSuperRegion())
        {
            neighbors_same_region ++;
        }
    }
    Region aux_region;
    for (int i = 0; i < super_region_size; ++ i)
    {
        Region my_region = regions->at(super_region.getRegion(i));
        if( Map::getInstance().isFromWasteland(my_region.getID()))
        {
            wastes ++;
        }

        for (int j = 0; j < my_region.getNbNeighbors(); ++ j)
        {
            aux_region = regions->at(my_region.getNeighbor(j));
            if (aux_region.getSuperRegion() != region.getSuperRegion() &&
                supers.count(aux_region.getSuperRegion()) == 0 ) {
                super_neighbors ++;
                supers.insert(std::pair<int,int>(aux_region.getSuperRegion(),0));
            }
        }
    }
    if (wastes)
    {
        return 0;
    }
    if( super_region_size == neighbors_same_region && super_region_size <= 3 && bonus > 0)
    {
        return 1<<30; //O chestie mare
    }
    return 0.7 * (bonus/super_region_size) + 0.2 * ( 1 / super_neighbors) + 0.1 / (1 + super_region_size - neighbors_same_region);

}
/*The function used to pick a region
  It will choose for a SuperRegion near some others
  SuperRegions I already have. (not sure if is a good starting)
  TO DO: work on it*/
int Pick::pickRegion(std::vector<int> startingRegionsReceived) 
{
    std::vector<int> alreadyPicked = Map::getInstance().getStartingSuperRegions();
    //I dont have already a region
    if(alreadyPicked.size() == 0 )
    {
        return rankingSearchForBestRegion(startingRegionsReceived);
    }
    //Search if i can help that region
    for( size_t i = 0; i < alreadyPicked.size(); ++i ) {
        //I iterate for direct access to the first region picked
        for( size_t j = 0; j < startingRegionsReceived.size(); ++j) {
            Region r = (*Map::getInstance().getRegions())[startingRegionsReceived[j]];
            if(Map::getInstance().hasStraightAccess(alreadyPicked[i],r.getID()) && (!Map::getInstance().isFromWasteland(r.getID()))) {
                Map::getInstance().addStartingRegion(r.getID());
                return r.getID();
            }
        }
        //I iterate for a SuperRegion in the neighbors
        for( size_t j = 0; j < startingRegionsReceived.size(); ++j) {
            Region r = (*Map::getInstance().getRegions())[startingRegionsReceived[j]];
            if(Map::getInstance().areSuperNeighbors(alreadyPicked[i],r.getSuperRegion()) && (!Map::getInstance().isFromWasteland(r.getID()))) {
                Map::getInstance().addStartingRegion(r.getID());
                return r.getID();
            }
        }
    }
    return rankingSearchForBestRegion(startingRegionsReceived);
}
/*The search based on the ranking system writen*/
int Pick::rankingSearchForBestRegion(std::vector<int> startingRegionsReceived) {
    int best_region = startingRegionsReceived[0];
    double max_score = getScore(startingRegionsReceived[0]);
    for (unsigned i = 1; i < startingRegionsReceived.size(); ++ i)
    {   
        double score = getScore(startingRegionsReceived.at(i));
        if (score > max_score)
        {
            max_score = score;
            best_region = startingRegionsReceived[i];
        }
    }
    Map::getInstance().addStartingRegion(best_region);
    return best_region;
}