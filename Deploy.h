#ifndef DEPLOY_H
#define DEPLOY_H

//stl
#include <string>
#include <stdio.h>
#include <stack>
//proiect

#include "Region.h"
#include "SuperRegion.h"

class AiEngine;

/**
 * This is the deploy engine and has methods specialized in deploing
 * armies for a task. Inform the AiEngine for all the task it assign to the armies
 * deployed
 */
class Deploy: boost::noncopyable
{
public:
	Deploy(AiEngine* AI);
	virtual ~Deploy();
	/** Utility procedures */
	void resetDeploy();
	int getSuplyLeft(Region region);
	void setDisponibleArmies(const int& nbArmies);
	int getNecessaryArmiesToConquerRegion(Region region);
	std::stack<int> getSuperRegionsToExtendTo();

	/** Functions used to deploy armies. Return all the deploies as pairs of
	 *  deploy zone and number of armies deployed
	 */
	std::vector<std::pair<int,int> > deployArmiesForExtend();	
	std::vector<std::pair<int,int> > deployArmiesForDefend();
	std::vector<std::pair<int,int> > deployArmiesForAttackEnemies();
	std::vector<std::pair<int,int> > deployArmiesForBackup();
	std::vector<std::pair<int, int> > deployLastArmies();
	

private:
	Deploy();
	std::vector<int> enemyDangerZones(const int& enemy);
	int checkIfReinforcementCouldSuply(const int& myRegion);
	double getSuperRegionsToExtendToValue(const SuperRegion &);
	int getEnemyToDefendAt(std::set<int> enemies, int index);
	/** The number of enemy zones where i already defended. It helps 
	 * to prioritise where to defend
	 */
	int enemies_defended;
	/** Armies left to be deployed this turn */
	int m_disponible_armies;
	/** Poiner at the AiEngine */
	AiEngine *p_ai;

};

#endif