#ifndef BATTLE_H
#define BATTLE_H

#include "Fight.h"

class AiEngine;

/**
 * The clased used for decisions of attack. Do the math and attack efficiently
 * an enemy or neutral region
 */
class Battle: boost::noncopyable
{
public:
	Battle(AiEngine* AI);
	virtual ~Battle();
	std::vector<std::vector<int> > attackForExtend(std::vector<std::vector<int> > schedueledForAttack);
	std::vector<std::vector<int> > multyAttack();

private:
	Battle();
	/** Pointer at the AiEngine */
	AiEngine* p_ai;
	Fight f;
};


#endif