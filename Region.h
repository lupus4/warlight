#ifndef REGION_H
#define REGION_H

//stl
#include <vector>
#include <string>

// project
#include "main.h"

/** 
 * The class wich describe a Region by armies, owner, waseland 
 * characteristic
 */
class Region
{
public:
	Region();
	Region(const int& pId, const  int& superRegion);
	virtual ~Region();

	void addNeighbor(const int& neighbor);
	void setArmies(const int& nbArmies) { armies = nbArmies; }
	void setOwner(const Player& pOwner){ owner = pOwner; }

	inline int getArmies() const { return armies; }
	inline Player getOwner() const { return owner; }
	inline int getSuperRegion() const { return superRegion; }
	int getNeighbor(const size_t& index) const ;
	int getNbNeighbors() const;
    int getID() { return id; }
    bool isNeighbor(int Region);
    bool isWasteland() { return m_is_wasteland; }
    void setWasteland() {m_is_wasteland = true;}

private:
	std::vector<int> neighbors;
	int id;
	int superRegion;
	Player owner;
	int armies;
	bool m_is_wasteland;
};

#endif // REGION_H
