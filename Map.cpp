//stl
#include <iostream>
#include <set>
#include <vector>
#include <queue>

//project
#include "Region.h"
#include "Map.h"
#include "SuperRegion.h"
#include "Bot.h"


void Map::init(std::vector<Region>& regions, 
               std::vector<int>& ownedRegions,
               std::vector<SuperRegion>& superRegions)
{
    m_regions = &regions;
    m_myRegions = &ownedRegions;
    m_superRegions = &superRegions;
}

std::vector<int> Map::getStartingSuperRegions()
{
    return m_myStartingSuperRegions;
}

void Map::addStartingRegion(int pickedRegion)
{
    (*m_regions)[pickedRegion].setOwner(ME);
    m_myStartingSuperRegions.push_back((*m_regions)[pickedRegion].getSuperRegion());
}

/**
 * Function used to find the shortest path to an enemy region
 * (neutral or opponent)-- work on it( maybe it is better to diferenciate
 * them somehow). Use a bfs algorithm and return the destination
 * of transfering all the armies from the specified region
 */
int Map::moveToRegion(const int start)
{

    std::vector<bool> visited(m_regions->size(), false);
    std::vector<int> path(m_regions->size(), 0);
    std::queue<int> proc;
    proc.push(start);
    visited[start] = true;
    while(!proc.empty())
    {
        Region currentRegion = (*m_regions)[proc.front()];
        int neighborNr = currentRegion.getNbNeighbors();
        for (int i = 0; i < neighborNr; i++)
        {
            int nbIndex = currentRegion.getNeighbor(i);
            if (!visited[nbIndex])
            {
                visited[nbIndex] = true;
                path[nbIndex] = currentRegion.getID();
                proc.push(nbIndex);
                if ((*m_regions)[nbIndex].getOwner() != ME)
                {
                    while (path[nbIndex] != start)
                    {
                        nbIndex = path[nbIndex];
                    }
                    return nbIndex;
                }
            }
        }
        proc.pop();
    }
    return start;
}

/**
 * Return the indexes of the enemy Regions neighbors with one of my
 * regions
 */
std::set<int> Map::getEnemyNeighbors()
{
    std::set<int> enemyNeighborReg;
    for (auto v : (*m_myRegions))
    {
       Region currentRegion = (*m_regions)[v]; 
       int neighborNr = currentRegion.getNbNeighbors();
       for (int i = 0; i < neighborNr; i++)
       {
           int nbIndex = currentRegion.getNeighbor(i);
           if ((*m_regions)[nbIndex].getOwner() == ENEMY)
               enemyNeighborReg.insert(nbIndex); 
       }
    }
    return enemyNeighborReg;
}

/**
 * Get all my regions which can attack a neutral or enemy
 * region from a superRegion
 */
std::vector<Region> Map::getSuperRegNotMineNb(int sr)
{   
    std::vector<Region> ret;
    std::set<int> reg;
    for (auto r : (*m_myRegions))
    {
        Region currentRegion = (*m_regions)[r];
        int neighborNr = currentRegion.getNbNeighbors();
        for (int i = 0; i < neighborNr; i++)
        {
            int nbIndex = currentRegion.getNeighbor(i); 
            Region neighbor = (*m_regions)[nbIndex];
            if (neighbor.getSuperRegion() == sr && neighbor.getOwner() != ME)
                reg.insert(r); 
        }
    }
    for (auto v : reg)
        ret.push_back((*m_regions)[v]);
    return ret;
}

/**
 * Get the enemy or neutral regions from a superregion
 * We can use this to extend 
 */
std::vector<Region> Map::getEmptyFromSuperReg(int sr)
{
    std::vector<Region> regions;
    SuperRegion superReg = (*m_superRegions)[sr];
    for (size_t i = 0; i < superReg.size(); i++)
    {
        Region currentRegion = (*m_regions)[superReg.getRegion(i)];
        if (currentRegion.getOwner() != ME)
            regions.push_back(currentRegion);
    }
    return regions;
}

/**
 * Get the superRegions where I have at least one region
 */
std::vector<SuperRegion> Map::getMyPartialSuperRegions()
{
    std::vector<SuperRegion> sr;
    for (size_t i = 0; i < (*m_superRegions).size(); i++)
    {
        int regionNr = (*m_superRegions)[i].size();
        for (int j = 0; j < regionNr; j++)
        {
            int region = (*m_superRegions)[i].getRegion(j);
            if ((*m_regions)[region].getOwner() == ME)
            {
                sr.push_back((*m_superRegions)[i]);
                break;
            }
        }
    }
    return sr;
}

/**
 * Method wich determine if a region has straight access to a super region.
 * It helps a lot in picking phase 
 */
bool Map::hasStraightAccess(int super_region, int region) 
{
    Region my_region = (*m_regions)[region];
    for( int i = 0; i < my_region.getNbNeighbors(); ++ i)
    {
        Region neighbor = (*m_regions)[my_region.getNeighbor(i)];
        if(neighbor.getSuperRegion() == super_region && neighbor.getOwner() == NEUTRAL)
        {
            return true;
        }
    }
    return false;
}

/** Method to decide if two super regions are neighbors */
bool Map::areSuperNeighbors(int super_region1, int super_region2)
{
    SuperRegion sr1 = (*m_superRegions)[super_region1];
    SuperRegion sr2 = (*m_superRegions)[super_region2];
    for( size_t i = 0; i < sr1.size(); ++ i) 
    {
        for( size_t j = 0; j < sr2.size(); ++ j)
        {
            if((*m_regions)[sr1.getRegion(i)].isNeighbor(sr2.getRegion(j)))
            {
                return true;
            }
        }
    }
    return false;
}

/** Check if a region is from an wasteland zone*/
bool Map::isFromWasteland(int region)
{
    Region r = (*m_regions)[region];
    SuperRegion sr = (*m_superRegions)[r.getSuperRegion()];
    return sr.hasWasteland();
}

/**
 * WTF I DID HERE??
 * HELP??
 */
std::vector<std::vector<Region> > Map::getMyExteriorRegions()
{
    bool found;
    std::vector<std::vector<Region> > solution;
    std::vector<SuperRegion> mySuperRegions = getMyPartialSuperRegions();
    for( size_t i = 0; i < m_myRegions->size(); ++ i ) 
    {
        std::vector<Region> result;
        Region myRegion = (*m_regions)[(*m_myRegions)[i]];
        result.push_back(myRegion);
        for( int j = 0; j < myRegion.getNbNeighbors(); ++j)
        {   
            found = false;
            Region neighbor = (*m_regions)[myRegion.getNeighbor(j)];
            for( size_t k = 0; k < mySuperRegions.size(); k++ )
            {

                if(mySuperRegions[k].getID() == neighbor.getSuperRegion())
                {
                    found = true;
                    break;
                }
            }
            if(!found)
            {
                result.push_back(neighbor);
            }
        }
        if(result.size() > 1)
        {
            solution.push_back(result);
        }
    }
    return solution;
}

/**
 * Get my Regions which have as neighbor at least a zone not owned by me
 * and return for each of them the enemies regions at the borders
 */
std::vector<std::vector<Region> >Map::getMyConflictZones()
{
    bool found;
    std::vector<std::vector<Region> > solution;
    for( size_t i = 0; i < m_myRegions->size(); ++ i ) 
    {
        found = false;
        std::vector<Region> result;
        Region myRegion = (*m_regions)[(*m_myRegions)[i]];
        result.push_back(myRegion);
        for( int j = 0; j < myRegion.getNbNeighbors(); ++j)
        {   
            Region neighbor = (*m_regions)[myRegion.getNeighbor(j)];
            if(neighbor.getOwner() != ME)
            {
                result.push_back(neighbor);
                found = true;
                break;
            }
        }
        if(found)
        {
           solution.push_back(result);
        }
    }
    return solution;
}

/**
 * Return my regions which have all the neighbors owned by me.
 * Those regions need to transfer all the armies to the exterior
 * (fighting zones)
 */
std::vector<Region> Map::getRegionsToTransfer()
{
    std::vector<Region> toTransfer;
    for( size_t i = 0; i < m_myRegions->size(); ++ i )
    {
        bool valid = true;
        Region myRegion = (*m_regions)[(*m_myRegions)[i]];
        for( int j = 0; j < myRegion.getNbNeighbors(); ++j)
        {
            Region neighbor = (*m_regions)[myRegion.getNeighbor(j)];
            if(neighbor.getOwner() != ME)
            {
                valid = false;
                break;
            }
        }
        if(valid)
        {
            toTransfer.push_back(myRegion);
        }
    }
    return toTransfer;
}

/**
 * Return for each zone that will be transfered, the destination(the closest region
 * to an enemy). It is used with the function above
 */
std::vector<std::pair<Region, Region> > Map::destinationForTransfer()
{
    std::vector<std::pair<Region,Region> > destinations;
    std::vector<Region> toTransfer = getRegionsToTransfer();
    for(size_t i = 0; i < toTransfer.size(); ++i)
    {
        int destination = moveToRegion(toTransfer[i].getID());
        std::pair<Region,Region> p(toTransfer[i],(*m_regions)[destination]);
        destinations.push_back(p);
    }
    return destinations;
}

/**
 * Check if i have all the Regions for a SuperRegion
 */
bool Map::totalyMySuperRegion(int sr)
{
    SuperRegion super_region = (*m_superRegions)[sr];
    for( size_t i = 0; i < super_region.size(); ++i)
    {
        if((*m_regions)[super_region.getRegion(i)].getOwner() != ME)
            return false;
    }
    return true;
}

/**
 * Return the number of the regions i din not complete of conquering yet
 */
int Map::nonCompletedSuperRegions()
{
    int nonCompleted = 0;
    std::vector<SuperRegion> my_partial_super_regions = getMyPartialSuperRegions();
    for( size_t i = 0; i < my_partial_super_regions.size(); ++ i)
    {
        if(!totalyMySuperRegion(my_partial_super_regions[i].getID())) {
            nonCompleted++;
        }
    }
    return nonCompleted;
}