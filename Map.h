#ifndef MAP_H
#define MAP_H
//stl
#include <iostream>
#include <set>
#include <vector>

//project
#include "Region.h"
#include "SuperRegion.h"

/**
 * A singleton class which provides tehnical details about
 * regions and superregions charactheristics. We have here refernces
 * at the vectors of Regions and SuperRegions from the Bot class
 */
class Map
{
private:
    Map(){}
    Map(const Map&) = delete;
    void operator=(const Map&) = delete;

public:
    static Map& getInstance() 
    {
        static Map instance;
        return instance;
    }
    /*Method used to initialize Map by referencing Regions and Super
      regions vectors*/
    void init(std::vector<Region>& regions, 
              std::vector<int>& ownedRegions,
              std::vector<SuperRegion>& superRegions);

    int moveToRegion(const int start);
    void addStartingRegion(int pickedSuperRegion);

    /* Function which offers informations about the regions/superRegions with
     * specified charactheristics */ 
    std::set<int> getEnemyNeighbors(); 
    std::vector<Region> getSuperRegNotMineNb(int sr);
    std::vector<Region> getEmptyFromSuperReg(int sr);
    std::vector<SuperRegion> getMyPartialSuperRegions();
    std::vector<std::vector<Region> > getMyExteriorRegions();
    std::vector<std::vector<Region> > getMyConflictZones();
    std::vector<Region> getRegionsToTransfer();

    /* Access to the private fields of the class*/
    std::vector<Region>* getRegions() { return m_regions; }
    std::vector<SuperRegion>* getSuperRegions() { return m_superRegions; }
    std::vector<int>* getMyRegions() { return m_myRegions; }
    std::vector<int> getStartingSuperRegions();

    /* Offers informations about a region or a superRegion */
    bool hasStraightAccess(int super_region, int region);
    bool areSuperNeighbors(int super_region1, int super_region2);
    bool isFromWasteland(int region);
    bool totalyMySuperRegion(int sr);
    int nonCompletedSuperRegions();
 
    std::vector<std::pair<Region, Region> > destinationForTransfer();    

private:
    /** The vector of all the Regions on the map indexed by ID */
    std::vector<Region>* m_regions;
    /** The vector of all the SuperRegions */
    std::vector<SuperRegion>* m_superRegions;
    /** The vector of my Regions */
    std::vector<int>* m_myRegions;
    /** Vector of my first SuperRegions used for the picking phase.
     *  TO DO: get the opponent starting SUperRegions and work with it.
     *  We can't win without that information */
    std::vector<int> m_myStartingSuperRegions;
};

#endif
