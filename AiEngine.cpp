#include "AiEngine.h"

AiEngine::AiEngine() : m_deployEngine(this), m_battleEngine(this)
{

}
AiEngine::~AiEngine()
{

}
/**
 * Schedule a Region for attacking an enemy
 */
void AiEngine::useToAttackEnemy(int source, int destination)
{
	std::vector<int> attack;
	attack.push_back(source);
	attack.push_back(destination);
	usedToAttackEnemy.push_back(attack);
}
/**
 * Schedule an enemy for attacking a neutral region
 * Used at extending
 */
void AiEngine::useToExtend(const int& myRegion, const int& neutralRegion)
{
	std::vector<int> attack;
	attack.push_back(myRegion);
	attack.push_back(neutralRegion);
	usedToExtend.push_back(attack);
}

/**
 * Mark some armies from a region as used
 */
void AiEngine::takeInitialSuply(const int& myRegion, const int& nbArmies)
{
	std::unordered_map<int,int>::const_iterator got = takenInitialSuply.find(myRegion);
	if( got == takenInitialSuply.end() ) {
		std::pair<int,int> p(myRegion, nbArmies);
		takenInitialSuply.insert(p);
		return;
	}
	std::pair<int,int> p(myRegion, got->second + nbArmies);
	takenInitialSuply.insert(p);
}
int AiEngine::getInitialSuplyUsed(const int& myRegion)
{
	std::unordered_map<int,int>::const_iterator got = takenInitialSuply.find(myRegion);
	if( got == takenInitialSuply.end() ) {
		return 0;
	}
	return got->second;

}
/** TO DO get suply from nearby Regions which can suply */
void AiEngine::askForSuply(const int& myRegion)
{
	(void)myRegion;
}

/**
 * The engine will deploy all the armies disponible using the deployEngine.
 * First all the containers with tasks are cleared and the deploy engine 
 * reseted. 
 * The round is incremented ater this apel.
 * All the results from the deploy functions are concatenated and
 * returned to the Bot.
 */
std::vector<std::pair<int, int> > AiEngine::deployArmies()
{
	//std::cerr<<"Round "<<round<<std::endl;
	m_deployEngine.setDisponibleArmies(m_nbArmies);
	usedToExtend.clear();
	usedToAttackEnemy.clear();
	takenInitialSuply.clear();
	safeRegions.clear();
	transfer.clear();
	m_deployEngine.resetDeploy();

	std::vector<std::pair<int,int> > deployForDefend= m_deployEngine.deployArmiesForDefend();

	std::vector<std::pair<int,int> > deployForExtend = m_deployEngine.deployArmiesForExtend();
	for( size_t i = 0; i < deployForExtend.size(); ++i)
	{
		deployForDefend.push_back(deployForExtend[i]);
	}
	if(m_nbArmies > 0) 
	{
		std::vector<std::pair<int, int> > deployForAttack = m_deployEngine.deployArmiesForAttackEnemies();
		for( size_t i = 0; i < deployForAttack.size(); ++ i)
		{
			deployForDefend.push_back(deployForAttack[i]);
		}
	}
	if(m_nbArmies > 0)
	{
		std::vector<std::pair<int, int> > deployForBackUp = m_deployEngine.deployArmiesForBackup();
		for( size_t i = 0; i < deployForBackUp.size(); ++ i )
		{
			deployForDefend.push_back(deployForBackUp[i]);
		}
	}
	if(m_nbArmies > 0)
	{
		std::vector<std::pair<int, int> > lastDeploy = m_deployEngine.deployLastArmies();
		for( size_t i = 0; i < lastDeploy.size(); ++ i )
		{
			deployForDefend.push_back(lastDeploy[i]);
		}
	}
	round++;
	return deployForDefend;
}

/**
 * This method give the vector with tasks for attack to 
 * the battleEngine and concatenate results.
 * Also this method transfer all the regions which are surrouned
 * by friendly zones to the nearest enemy
 */
std::vector<std::vector<int> > AiEngine::attack()
{
	for( size_t i = 0; i < usedToAttackEnemy.size(); ++ i)
	{
		usedToExtend.push_back(usedToAttackEnemy[i]);
	}
	std::vector<std::vector<int> > extend = m_battleEngine.attackForExtend(usedToExtend);
	for( size_t i = 0; i < extend.size(); i++ ) {
		transfer.push_back(extend[i]);
	}
	std::vector<std::vector<int> > multyAttack = m_battleEngine.multyAttack();
	for( size_t i = 0; i < multyAttack.size(); ++ i)
	{
		transfer.push_back(multyAttack[i]);
		//std::cerr<<"I attack "<<multyAttack[i][1]<<" from "<<multyAttack[i][0]<<std::endl;
	}
	std::vector<std::pair<Region, Region> > commingToExterior = Map::getInstance().destinationForTransfer();
	for( size_t i = 0; i < commingToExterior.size(); i++)
	{
		Region source = commingToExterior[i].first;
		int armies = source.getArmies() - getInitialSuplyUsed(source.getID()) - 1;
		if(armies <= 0)
			continue;
		std::vector<int> transferProp;
		transferProp.push_back(source.getID());
		transferProp.push_back(commingToExterior[i].second.getID());
		transferProp.push_back(armies);
		transfer.push_back(transferProp);
		//std::cerr<<"I transfer "<<source.getID()<<" to "<<commingToExterior[i].second.getID()<<std::endl;
	}

	return transfer;
}

void AiEngine::markAsSafe(const int& region)
{
	safeRegions.insert(region);
}

bool AiEngine::isSafe(const int& region)
{
	return safeRegions.find(region) != safeRegions.end();
}

void AiEngine::addSuply(std::vector<int> suply)
{
	transfer.push_back(suply);
}
