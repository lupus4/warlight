#include <unordered_map>
#include <climits>

#include "AiEngine.h"
#include "Battle.h"
#include "Map.h"

Battle::Battle(AiEngine* AI): 
	p_ai(AI)
{

}
Battle::~Battle()
{

}
/**
 * This method recieve a vector of sources and destinations scheduled for attack and return a
 * vector with the regions which can attack and with the numbers of armies needed to win the
 * fight
 */
std::vector<std::vector<int> > Battle::attackForExtend(std::vector<std::vector<int> > schedueledForAttack)
{
	std::unordered_map<int, int> armiesTaken;
	std::vector<std::vector<int> > solution;
	for( size_t i = 0; i < schedueledForAttack.size(); i++ )
	{
		int my = schedueledForAttack[i][0];
		int opponent = schedueledForAttack[i][1];
		Region my_region = (*Map::getInstance().getRegions())[my];
		Region enemy = (*Map::getInstance().getRegions())[opponent];
		int bonus = 0;
		if(enemy.getOwner() == ENEMY)
		{
			bonus += p_ai->enemy_expected_deploy;
		}
		int armiesNeeded = f.getAttackersNeededToCapture(enemy.getArmies() + bonus);
		int armiesDisponible = my_region.getArmies() - 1;
		int armiesUsed = 0;
		std::unordered_map<int,int>::const_iterator got = armiesTaken.find(my_region.getID());
		if(got == armiesTaken.end())
		{
			armiesUsed = 0;
		} else {
			armiesUsed = got->second;
		}
		armiesDisponible -= armiesUsed;
		if(armiesDisponible >= armiesNeeded)
		{
			armiesTaken.insert(std::make_pair<int,int>(my_region.getID(), armiesUsed + armiesNeeded));
			std::vector<int> attak;
			attak.push_back(my_region.getID());
			attak.push_back(enemy.getID());
			attak.push_back(armiesNeeded);
			solution.push_back(attak);
		}
	}
	return solution;
}

/**
 * This function land multiples attacks into the nearby zones owned by enemies
 * It is used after finishing extending and get the bot into new SuperRegions
 */
std::vector<std::vector<int> > Battle::multyAttack()
{
	Fight f;
	std::vector<std::vector<int> > solution;
	std::vector<std::vector<Region> > my_conflict_zones = Map::getInstance().getMyConflictZones();
	for(size_t i = 0; i < my_conflict_zones.size(); ++i)
	{
		std::vector<Region> possibleAttack = my_conflict_zones[i];
		int minimum_defended_enemy = INT_MAX;
		int neutral_target = 0;
		int minimum_defended_neutral = INT_MAX;
		int enemy_target = 0;
		int my_armies = possibleAttack[0].getArmies() - p_ai->getInitialSuplyUsed(possibleAttack[0].getID()) - 1;
		if(!Map::getInstance().totalyMySuperRegion(possibleAttack[0].getSuperRegion()))
			continue;
		if(Map::getInstance().nonCompletedSuperRegions() > 0)
			continue;
		for( size_t j = 1; j < possibleAttack.size(); ++j)
		{
			Region target = possibleAttack[j];
			if(target.getSuperRegion() == possibleAttack[0].getSuperRegion())
				continue;
			if(target.getOwner() == NEUTRAL)
			{
				if(target.getArmies() < minimum_defended_neutral)
				{
					minimum_defended_neutral = target.getArmies();
					neutral_target = target.getID();
				}
			} else {
				if(target.getArmies() < minimum_defended_enemy)
				{
					minimum_defended_enemy = target.getArmies();
					enemy_target = target.getID();
				}
			}
		}
		if( enemy_target > 0)
		{
			if(f.getAttackersNeededToCapture(minimum_defended_enemy + p_ai->enemy_expected_deploy) <= my_armies)
			{
				std::vector<int> attack;
				attack.push_back(possibleAttack[0].getID());
				attack.push_back(enemy_target);
				attack.push_back(my_armies);
				solution.push_back(attack);
				continue;
			}
		}
		if( neutral_target == 0)
			continue;
		if(f.getAttackersNeededToCapture(minimum_defended_neutral) <= my_armies)
		{
			std::vector<int> attack;
			attack.push_back(possibleAttack[0].getID());
			attack.push_back(neutral_target);
			attack.push_back(my_armies);
			solution.push_back(attack);
		}

	}
	return solution;
}